package com.cmcc.hackson3;

import org.apache.commons.lang3.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.cmcc.hackson3.main.Message;
import com.cmcc.hackson3.main.Producer;
import com.cmcc.hackson3.user.QueueManager;

@SpringBootApplication
public class Hackson3Application {
	private static final Logger logger = LoggerFactory.getLogger(Hackson3Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Hackson3Application.class, args);
		logger.info("start ========");

		//
		Thread r = new Thread(null, new Runnable() {

			@Override
			public void run() {
				while (true) {
					
					try {
						System.out.println(QueueManager.INS.stat());
						Thread.sleep(1000);
					} catch (InterruptedException e) {

					}
					//内存溢出时能及时报错，主线程退出
					for (int i = 0; i < 50; i++) {
						new Message(RandomUtils.nextInt(5, 60), "OutOfMemoryError");
					}
				}
			}
		}, "Main");
		r.start();

		for (int i = 0; i < 5; i++) {
			Producer producer = new Producer("Hackson-");
			producer.start();
		}
	}
}
