package com.cmcc.hackson3.main;

public final class Message {

	long terminalTime;  //消息最后的消费时间，超过此时间不再被消费，会被丢弃
	
	String phone;
	Long[] data = new Long[12500]; //点内存，当积压大量消费时，让程序快速内存溢出。
	long produceTime; //消息生产时间
	long consumeTime;//消息被消费的时间
	String discardmsg; //丢弃消息时要设置原因
	
	
	public Message(int lifetime,String phone) {
		produceTime = System.currentTimeMillis();
		terminalTime = produceTime+1000*lifetime;
		this.phone = phone;
	}

	public long getTerminalTime() {
		return terminalTime;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public long getProduceTime() {
		return produceTime;
	}

	void setProduceTime(long produceTime) {
		this.produceTime = produceTime;
	}

	public long getConsumeTime() {
		return consumeTime;
	}

	void setConsumeTime(long consumeTime) {
		this.consumeTime = consumeTime;
	}

	public String getDiscardmsg() {
		return discardmsg;
	}

	public void setDiscardmsg(String discardmsg) {
		this.discardmsg = discardmsg;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Message [terminalTime=");
		builder.append(terminalTime);
		builder.append(", phone=");
		builder.append(phone);
		builder.append("]");
		return builder.toString();
	}
	
	
}
