package com.cmcc.hackson3.main;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

public enum RocketMQ {
	INS;
	
	private static final ConcurrentHashMap<String, LinkedBlockingQueue<Message>> queueMap =
			new ConcurrentHashMap();
	
	LinkedBlockingQueue<Message> getQueue(String topic){
		LinkedBlockingQueue<Message> queue = queueMap.get(topic);
		if(queue==null) {
			synchronized (this) {
				LinkedBlockingQueue<Message> queueinner = queueMap.get(topic);
				if(queueinner==null) {
					LinkedBlockingQueue<Message> instance = new LinkedBlockingQueue<Message>();
					queueMap.put(topic, instance);
					return instance;
				}else {
					return queueinner;
				}
			}
		}else {
			return queue;
		}
	}
	
	/**
	 * 返回各个队列中积压的数据量
	 * 
	 */
	public Map<String ,Long> countBacklog(){
		Map result = new HashMap<String ,Long>();
		for(Entry<String, LinkedBlockingQueue<Message>> entry : queueMap.entrySet()) {
			result.put(entry.getKey(), entry.getValue().size());
		}
		return result;
	}
	
	public void destroyQueue(String topic) {
		queueMap.remove(topic);
	}
}
