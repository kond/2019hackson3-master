package com.cmcc.hackson3.user;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cmcc.hackson3.main.Message;
import com.cmcc.hackson3.main.RocketMQ;


/**
 * 请参赛者完成下面类的功能:
 * <br/>
 * 完成消费者和队列管理工具的代码，实现消息及时消费，节省消费者个数；打印日志准确【程序每秒打一次日志，日志内容为：总生产速度，总消费速度，各个被使用的队列的积压数据量，消费者个数】
 * 
 */
public enum QueueManager {
	INS;
	private static final Logger logger = LoggerFactory.getLogger(QueueManager.class);

	/**
	 *            生产者每产生一个消息，保存入队列后将调用此方法一次
	 * 
	 * @param topic
	 *            存入队列的队列名
	 * @param msg
	 *            保存的消息
	 */

	public void inqueue(String topic, Message msg) {
	}

	/**
	 * 消费者每消费一个消息，将调用此方法一次
	 * 
	 * @param topic
	 *            队列名
	 * @param msg
	 *            消费的消息
	 */
	public void outqueue(String topic, Message msg) {
	}
	
	/**
	 * 超时丢弃后，调用该方法
	 * 
	 * @param topic
	 *            队列名
	 * @param msg
	 *            消费的消息
	 */
	public void discard(String topic, Message msg) {
		logger.warn("discard topic:{},{}",topic,msg);
	}

	/**
	 * 根据消息特点，为该消息选择一个Topic入队
	 * 
	 */
	public String chooseTopic(Message msg)
	{
		return "Default";
	}

	/**
	 *打印系统生产和消费的状态信息，每一秒会被调用一次
	 * 
	 */
	public String stat() {
		Map<String ,Long> map = RocketMQ.INS.countBacklog();
		StringBuffer sb = new StringBuffer();
		sb.append("总生产速度:").append(0)
		  .append(",总消费速度:").append(0)
		  .append(",队列的积压:").append(map.toString())
		  .append("消费者个数:").append(0);
		return sb.toString();
	}
	
	/**
	 *生产者启动时，会调用一次该方法。以方便统计启动了几个生产者
	 * 
	 */
	public void producerStarted(String producerName) {
		logger.warn("producerStarted Name:{}",producerName);
	}
	
	/**
	 *消费者启动时，会调用一次该方法。以方便统计启动了那个Topic消费者
	 * 
	 */
	public void consumerStarted(String topic) {
		logger.warn("consumerStarted topic:{}",topic);
	}
}
